//
//  Data.swift
//  search-store
//
//  Created by Backend_1 on 8/10/16.
//  Copyright © 2016 Joonik. All rights reserved.
//

import Foundation

class Data {
    
    struct Brand {
        var title: String
    }
    
    struct  Clothing{
        var title: String
    }
    
    var Brands = [
        Brand(title: "Banana Republic"),
        Brand(title: "Hugo Boss"),
        Brand(title: "Rebecca Taylor"),
        Brand(title: "Boss"),
        Brand(title: "Gap"),
        Brand(title: "Taylor"),
    ]
    
    var Clothings = [
        Clothing(title: "Denim"),
        Clothing(title: "Pants"),
        Clothing(title: "Sweaters"),
        Clothing(title: "Skirts"),
        Clothing(title: "Dresses"),
    ]
}