//
//  Extensions.swift
//  search-store
//
//  Created by Backend_1 on 8/10/16.
//  Copyright © 2016 Joonik. All rights reserved.
//

import UIKit

extension UINavigationController {
    public func presentTransparentNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), forBarMetrics:UIBarMetrics.Default)
        navigationBar.translucent = true
        navigationBar.shadowImage = UIImage()
        setNavigationBarHidden(false, animated:true)
    }
}

extension UIColor {
    func getColorFromRGB(value: String, alph: Float) -> UIColor {
        var cString:String = value.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        let r = CGFloat((rgbValue & 0xFF0000) >> 16)/255.0
        let g = CGFloat((rgbValue & 0xFF00) >> 8)/255.0
        let b = CGFloat((rgbValue & 0xFF))/255.0
        let myColor = UIColor(red:r, green: g, blue: b, alpha: CGFloat(alph))
        return myColor
    }
}