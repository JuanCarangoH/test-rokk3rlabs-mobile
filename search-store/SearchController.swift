//
//  ViewController.swift
//  search-store
//
//  Created by Backend_1 on 8/10/16.
//  Copyright © 2016 Joonik. All rights reserved.
//

import UIKit

struct Result {
    var title: String
    var words: String
}

class SearchController: UIViewController {

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var brands = Data().Brands
    var clothings = Data().Clothings
    var results = [
        Result(title: "Brands", words: ""),
        Result(title: "Clothing Type", words: ""),
        Result(title: "Result query", words: "")
    ]
    var resToSend = [Result]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.presentTransparentNavigationBar()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func didPressSubmit(sender: UIButton) {
        var query = txtSearch.text!
        
        results[0].words = ""
        for brand in brands {
            if (query.rangeOfString(brand.title) != nil) || (query.rangeOfString(brand.title.lowercaseString) != nil) {
                results[0].words += "\(brand.title) "
                query = query.stringByReplacingOccurrencesOfString(brand.title, withString: "", options: .CaseInsensitiveSearch, range: nil)
                query = query.stringByReplacingOccurrencesOfString("\\s+",withString: " ", options: .RegularExpressionSearch)
            }
        }
        results[0].words = results[0].words.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        results[1].words = ""
        for clothing in clothings {
            if (query.rangeOfString(clothing.title) != nil) || (query.rangeOfString(clothing.title.lowercaseString) != nil){
                results[1].words += "\(clothing.title) "
                query = query.stringByReplacingOccurrencesOfString(clothing.title, withString: "", options: .CaseInsensitiveSearch, range: nil)
                query = query.stringByReplacingOccurrencesOfString("\\s+",withString: " ", options: .RegularExpressionSearch)
            }
        }
        results[1].words = results[1].words.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        results[2].words = ""
        results[2].words += query.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        resToSend = []
        for res in results {
            if res.words != "" {
                resToSend.append(res)
            }
        }
        
        self.performSegueWithIdentifier("toResults", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toResults" {
            let ResultsVC = segue.destinationViewController as! ResultsController
            ResultsVC.results = self.resToSend
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

}

